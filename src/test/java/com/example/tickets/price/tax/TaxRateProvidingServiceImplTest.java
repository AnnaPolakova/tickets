package com.example.tickets.price.tax;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class TaxRateProvidingServiceImplTest {

    @InjectMocks
    private TaxRateProvidingServiceImpl taxRateProvidingServiceImpl;

    @Test
    public void getCurrentTaxRate() {
        BigDecimal currentTaxRate = taxRateProvidingServiceImpl.getCurrentTaxRate();
        assertEquals(currentTaxRate.compareTo(BigDecimal.valueOf(21)), 0);
    }
}