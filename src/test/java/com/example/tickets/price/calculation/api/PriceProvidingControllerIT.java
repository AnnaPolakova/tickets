package com.example.tickets.price.calculation.api;

import com.example.tickets.price.base.service.BasePriceProvidingServiceImpl;
import com.example.tickets.price.calculation.PassengerType;
import com.example.tickets.price.calculation.dto.input.PassengerDTO;
import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class PriceProvidingControllerIT {

    @Autowired
    private MockMvc mockMvc;

    private List<RoutePassengerListDTO> routeListWithPassengers = new ArrayList<>();

    @Before
    public void setUp() {
        PassengerDTO adultPassengerLithuania = new PassengerDTO(PassengerType.ADULT, 2);
        PassengerDTO childPassengerLithuania = new PassengerDTO(PassengerType.CHILD, 3);
        List<PassengerDTO> passengerListLithuania = List.of(adultPassengerLithuania, childPassengerLithuania);
        RoutePassengerListDTO routePassengerListLithuania = new RoutePassengerListDTO(BasePriceProvidingServiceImpl.VILNUS_LITHUANIA, passengerListLithuania);

        PassengerDTO adultPassengerEstonia = new PassengerDTO(PassengerType.ADULT, 1);
        PassengerDTO childPassengerEstonia = new PassengerDTO(PassengerType.CHILD, 0);
        List<PassengerDTO> passengerListEstonia = List.of(adultPassengerEstonia, childPassengerEstonia);
        RoutePassengerListDTO routePassengerListEstonia = new RoutePassengerListDTO(BasePriceProvidingServiceImpl.TALLIN_ESTONIA, passengerListEstonia);

        routeListWithPassengers.add(routePassengerListLithuania);
        routeListWithPassengers.add(routePassengerListEstonia);
    }

    @Test
    public void getTicketPrices() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post("/prices")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(routeListWithPassengers)))
                .andExpect(status().isOk())
                // Lithuania
                .andExpect(jsonPath("$.route_passenger_price_list.[0].base_price.route_name").value(BasePriceProvidingServiceImpl.VILNUS_LITHUANIA))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].base_price.route_price").value("10.0"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[0].passenger.passenger_type").value("ADULT"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[0].passenger.luggage_count").value("2"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[0].ticket_price").value("12.1"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[0].luggage_price").value("7.26"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[0].total_price").value("19.36"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[1].passenger.passenger_type").value("CHILD"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[1].passenger.luggage_count").value("3"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[1].ticket_price").value("6.05"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[1].luggage_price").value("10.89"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].passenger_price_list.[1].total_price").value("16.94"))
                .andExpect(jsonPath("$.route_passenger_price_list.[0].total_route_price").value("36.3"))
                // Estonia
                .andExpect(jsonPath("$.route_passenger_price_list.[1].base_price.route_name").value(BasePriceProvidingServiceImpl.TALLIN_ESTONIA))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].base_price.route_price").value("20.0"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[0].passenger.passenger_type").value("ADULT"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[0].passenger.luggage_count").value("1"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[0].ticket_price").value("24.2"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[0].luggage_price").value("7.26"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[0].total_price").value("31.46"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[1].passenger.passenger_type").value("CHILD"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[1].passenger.luggage_count").value("0"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[1].ticket_price").value("12.1"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[1].luggage_price").value("0.0"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].passenger_price_list.[1].total_price").value("12.1"))
                .andExpect(jsonPath("$.route_passenger_price_list.[1].total_route_price").value("43.56"))
                // Total
                .andExpect(jsonPath("$.total_price").value("79.86"));
    }
}