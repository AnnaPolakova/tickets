package com.example.tickets.price.calculation.service;

import com.example.tickets.price.calculation.PassengerType;
import com.example.tickets.price.calculation.dto.input.PassengerDTO;
import com.example.tickets.price.calculation.dto.output.ResultPassengerPriceDTO;
import com.example.tickets.price.tax.TaxRateProvidingService;
import com.example.tickets.price.tax.TaxRateProvidingServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class PassengerPriceCalculationServiceImplTest {

    @InjectMocks
    private PassengerPriceCalculationServiceImpl passengerPriceCalculationServiceImpl;

    @Mock
    private TaxRateProvidingService taxRateProvidingService;

    @Test
    public void calculateForPassenger() {
        doReturn(TaxRateProvidingServiceImpl.CURRENT_TAX_RATE).when(taxRateProvidingService).getCurrentTaxRate();

        PassengerDTO adultPassenger = new PassengerDTO(PassengerType.ADULT, 2);
        ResultPassengerPriceDTO resultAdultPrice = passengerPriceCalculationServiceImpl.calculateForPassenger(BigDecimal.valueOf(10), adultPassenger);
        assertEquals(resultAdultPrice.getTicketPrice().compareTo(BigDecimal.valueOf(12.1)), 0);
        assertEquals(resultAdultPrice.getLuggagePrice().compareTo(BigDecimal.valueOf(7.26)), 0);
        assertEquals(resultAdultPrice.getTotalPrice().compareTo(BigDecimal.valueOf(19.36)), 0);

        PassengerDTO childPassenger = new PassengerDTO(PassengerType.CHILD, 3);
        ResultPassengerPriceDTO resultChildPrice = passengerPriceCalculationServiceImpl.calculateForPassenger(BigDecimal.valueOf(10), childPassenger);
        assertEquals(resultChildPrice.getTicketPrice().compareTo(BigDecimal.valueOf(6.05)), 0);
        assertEquals(resultChildPrice.getLuggagePrice().compareTo(BigDecimal.valueOf(10.89)), 0);
        assertEquals(resultChildPrice.getTotalPrice().compareTo(BigDecimal.valueOf(16.94)), 0);
    }
}