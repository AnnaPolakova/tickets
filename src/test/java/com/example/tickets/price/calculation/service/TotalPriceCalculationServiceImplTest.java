package com.example.tickets.price.calculation.service;

import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;
import com.example.tickets.price.calculation.dto.output.ResultRoutePassengerPriceListDTO;
import com.example.tickets.price.calculation.dto.output.ResultTotalPriceDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class TotalPriceCalculationServiceImplTest {

    @InjectMocks
    private TotalPriceCalculationServiceImpl totalPriceCalculationServiceImpl;

    @Mock
    private RoutePriceCalculationService routePriceCalculationService;

    @Mock
    private RoutePassengerListDTO routePassengerListLithuania;
    @Mock
    private RoutePassengerListDTO routePassengerListEstonia;

    @Mock
    private ResultRoutePassengerPriceListDTO resultRoutePassengerPriceListLithuania;
    @Mock
    private ResultRoutePassengerPriceListDTO resultRoutePassengerPriceListEstonia;

    @Test
    public void calculateTotal() {
        List<RoutePassengerListDTO> routeListWithPassengers = List.of(routePassengerListLithuania, routePassengerListEstonia);
        doReturn(resultRoutePassengerPriceListLithuania).when(routePriceCalculationService).calculateForRoute(routePassengerListLithuania);
        doReturn(BigDecimal.valueOf(36.3)).when(resultRoutePassengerPriceListLithuania).getTotalRoutePrice();
        doReturn(resultRoutePassengerPriceListEstonia).when(routePriceCalculationService).calculateForRoute(routePassengerListEstonia);
        doReturn(BigDecimal.valueOf(72.6)).when(resultRoutePassengerPriceListEstonia).getTotalRoutePrice();

        ResultTotalPriceDTO resultTotalPrice = totalPriceCalculationServiceImpl.calculateTotal(routeListWithPassengers);
        assertEquals(resultTotalPrice.getRoutePassengerPriceList().size(), 2);
        assertEquals(resultTotalPrice.getTotalPrice().compareTo(BigDecimal.valueOf(108.9)), 0);
    }
}