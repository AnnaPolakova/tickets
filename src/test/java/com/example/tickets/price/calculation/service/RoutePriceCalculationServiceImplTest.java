package com.example.tickets.price.calculation.service;

import com.example.tickets.price.base.dto.BasePriceDTO;
import com.example.tickets.price.base.service.BasePriceProvidingService;
import com.example.tickets.price.base.service.BasePriceProvidingServiceImpl;
import com.example.tickets.price.calculation.dto.input.PassengerDTO;
import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;
import com.example.tickets.price.calculation.dto.output.ResultPassengerPriceDTO;
import com.example.tickets.price.calculation.dto.output.ResultRoutePassengerPriceListDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class RoutePriceCalculationServiceImplTest {

    @InjectMocks
    private RoutePriceCalculationServiceImpl routePriceCalculationServiceImpl;

    @Mock
    private BasePriceProvidingService basePriceProvidingService;
    @Mock
    private PassengerPriceCalculationService passengerPriceCalculationService;

    @Mock
    private PassengerDTO adultPassenger;
    @Mock
    private PassengerDTO childPassenger;

    @Mock
    private BasePriceDTO basePrice;

    @Mock
    private ResultPassengerPriceDTO resultAdultPrice;
    @Mock
    private ResultPassengerPriceDTO resultChildPrice;

    @Test
    public void calculateForRoute() {
        List<PassengerDTO> passengerList = List.of(adultPassenger, childPassenger);
        RoutePassengerListDTO routePassengerList = new RoutePassengerListDTO(BasePriceProvidingServiceImpl.VILNUS_LITHUANIA, passengerList);
        doReturn(basePrice).when(basePriceProvidingService).getBasePriceByRouteName(BasePriceProvidingServiceImpl.VILNUS_LITHUANIA);
        BigDecimal routePrice = BigDecimal.valueOf(10);
        doReturn(routePrice).when(basePrice).getRoutePrice();
        doReturn(resultAdultPrice).when(passengerPriceCalculationService).calculateForPassenger(routePrice, adultPassenger);
        doReturn(BigDecimal.valueOf(19.360)).when(resultAdultPrice).getTotalPrice();
        doReturn(resultChildPrice).when(passengerPriceCalculationService).calculateForPassenger(routePrice, childPassenger);
        doReturn(BigDecimal.valueOf(16.940)).when(resultChildPrice).getTotalPrice();

        ResultRoutePassengerPriceListDTO resultRoutePassengerPriceList = routePriceCalculationServiceImpl.calculateForRoute(routePassengerList);

        BigDecimal actualRoutePrice = resultRoutePassengerPriceList.getBasePrice().getRoutePrice();
        assertEquals(actualRoutePrice.compareTo(routePrice), 0);

        assertEquals(resultRoutePassengerPriceList.getResultPassengerPriceList().size(), 2);

        BigDecimal actualTotalPrice = resultRoutePassengerPriceList.getTotalRoutePrice();
        assertEquals(actualTotalPrice.compareTo(BigDecimal.valueOf(36.3)), 0);
    }
}