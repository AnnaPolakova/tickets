package com.example.tickets.price.base.service;

import com.example.tickets.price.base.dto.BasePriceDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class BasePriceProvidingServiceImplTest {

    @InjectMocks
    private BasePriceProvidingServiceImpl basePriceProvidingServiceImpl;

    @Test
    public void getBasePriceByRouteName() {
        BasePriceDTO vilnusLithuaniaPrice = basePriceProvidingServiceImpl.getBasePriceByRouteName(BasePriceProvidingServiceImpl.VILNUS_LITHUANIA);
        assertEquals(vilnusLithuaniaPrice.getRouteName(), BasePriceProvidingServiceImpl.VILNUS_LITHUANIA);
        assertEquals(vilnusLithuaniaPrice.getRoutePrice().compareTo(BigDecimal.valueOf(10)), 0);

        BasePriceDTO tallinEstoniaPrice = basePriceProvidingServiceImpl.getBasePriceByRouteName(BasePriceProvidingServiceImpl.TALLIN_ESTONIA);
        assertEquals(tallinEstoniaPrice.getRouteName(), BasePriceProvidingServiceImpl.TALLIN_ESTONIA);
        assertEquals(tallinEstoniaPrice.getRoutePrice().compareTo(BigDecimal.valueOf(20)), 0);

        BasePriceDTO romeItalyPrice = basePriceProvidingServiceImpl.getBasePriceByRouteName(BasePriceProvidingServiceImpl.ROME_ITALY);
        assertEquals(romeItalyPrice.getRouteName(), BasePriceProvidingServiceImpl.ROME_ITALY);
        assertEquals(romeItalyPrice.getRoutePrice().compareTo(BigDecimal.valueOf(30)), 0);
    }
}