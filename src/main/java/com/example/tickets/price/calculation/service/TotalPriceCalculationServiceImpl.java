package com.example.tickets.price.calculation.service;

import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;
import com.example.tickets.price.calculation.dto.output.ResultRoutePassengerPriceListDTO;
import com.example.tickets.price.calculation.dto.output.ResultTotalPriceDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TotalPriceCalculationServiceImpl implements TotalPriceCalculationService {

    @Autowired
    private RoutePriceCalculationService routePriceCalculationService;

    @Override
    public ResultTotalPriceDTO calculateTotal(List<RoutePassengerListDTO> routeListWithPassengers) {

        List<ResultRoutePassengerPriceListDTO> resultRouteWithPassengersPriceList = routeListWithPassengers.stream()
                .map(routePassengerList -> routePriceCalculationService.calculateForRoute(routePassengerList))
                .collect(Collectors.toList());
        BigDecimal totalPrice = calculateTotalPrice(resultRouteWithPassengersPriceList);
        return new ResultTotalPriceDTO(resultRouteWithPassengersPriceList, totalPrice);
    }

    private BigDecimal calculateTotalPrice(List<ResultRoutePassengerPriceListDTO> resultRouteWithPassengersPriceList) {
        return resultRouteWithPassengersPriceList.stream()
                .map(resultRoutePassengerPriceList -> resultRoutePassengerPriceList.getTotalRoutePrice())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
