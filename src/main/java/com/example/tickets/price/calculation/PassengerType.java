package com.example.tickets.price.calculation;

public enum PassengerType {
    ADULT,
    CHILD
}
