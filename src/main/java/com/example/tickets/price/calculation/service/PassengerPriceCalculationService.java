package com.example.tickets.price.calculation.service;

import com.example.tickets.price.calculation.dto.input.PassengerDTO;
import com.example.tickets.price.calculation.dto.output.ResultPassengerPriceDTO;

import java.math.BigDecimal;

public interface PassengerPriceCalculationService {

    ResultPassengerPriceDTO calculateForPassenger(BigDecimal routePrice, PassengerDTO passenger);
}
