package com.example.tickets.price.calculation.api;

import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;
import com.example.tickets.price.calculation.dto.output.ResultTotalPriceDTO;
import com.example.tickets.price.calculation.service.TotalPriceCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/prices")
public class PriceProvidingController {

    @Autowired
    private TotalPriceCalculationService totalPriceCalculationService;

    @PostMapping
    public ResultTotalPriceDTO getTicketPrices(@RequestBody List<RoutePassengerListDTO> routeListWithPassengers) {
        return totalPriceCalculationService.calculateTotal(routeListWithPassengers);
    }
}
