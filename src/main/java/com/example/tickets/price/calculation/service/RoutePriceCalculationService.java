package com.example.tickets.price.calculation.service;

import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;
import com.example.tickets.price.calculation.dto.output.ResultRoutePassengerPriceListDTO;

public interface RoutePriceCalculationService {

    ResultRoutePassengerPriceListDTO calculateForRoute(RoutePassengerListDTO routePassengerList);
}
