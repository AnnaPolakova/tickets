package com.example.tickets.price.calculation.dto.output;

import com.example.tickets.price.calculation.dto.input.PassengerDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class ResultPassengerPriceDTO {

    @JsonProperty("passenger")
    PassengerDTO passenger;
    @JsonProperty("ticket_price")
    BigDecimal ticketPrice;
    @JsonProperty("luggage_price")
    BigDecimal luggagePrice;
    @JsonProperty("total_price")
    BigDecimal totalPrice;
}
