package com.example.tickets.price.calculation.service;

import com.example.tickets.price.base.dto.BasePriceDTO;
import com.example.tickets.price.base.service.BasePriceProvidingService;
import com.example.tickets.price.calculation.dto.input.PassengerDTO;
import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;
import com.example.tickets.price.calculation.dto.output.ResultPassengerPriceDTO;
import com.example.tickets.price.calculation.dto.output.ResultRoutePassengerPriceListDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoutePriceCalculationServiceImpl implements RoutePriceCalculationService {

    @Autowired
    private BasePriceProvidingService basePriceProvidingService;
    @Autowired
    private PassengerPriceCalculationService passengerPriceCalculationService;

    @Override
    public ResultRoutePassengerPriceListDTO calculateForRoute(RoutePassengerListDTO routePassengerList) {
        String routeName = routePassengerList.getRouteName();
        BasePriceDTO basePrice = basePriceProvidingService.getBasePriceByRouteName(routeName);
        List<ResultPassengerPriceDTO> resultPassengerPriceList = calculateForPassengers(basePrice.getRoutePrice(), routePassengerList.getPassengerList());
        BigDecimal totalPrice = calculateTotalPrice(resultPassengerPriceList);
        return new ResultRoutePassengerPriceListDTO(basePrice, resultPassengerPriceList, totalPrice);
    }

    private List<ResultPassengerPriceDTO> calculateForPassengers(BigDecimal routePrice, List<PassengerDTO> passengers) {
        return passengers.stream()
                .map(passenger -> passengerPriceCalculationService.calculateForPassenger(routePrice, passenger))
                .collect(Collectors.toList());
    }

    private BigDecimal calculateTotalPrice(List<ResultPassengerPriceDTO> resultPassengerPriceList) {
        return resultPassengerPriceList.stream()
                .map(resultPassengerPrice -> resultPassengerPrice.getTotalPrice())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
