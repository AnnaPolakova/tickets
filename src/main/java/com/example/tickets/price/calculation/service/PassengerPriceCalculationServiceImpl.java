package com.example.tickets.price.calculation.service;

import com.example.tickets.price.calculation.PassengerType;
import com.example.tickets.price.calculation.dto.input.PassengerDTO;
import com.example.tickets.price.calculation.dto.output.ResultPassengerPriceDTO;
import com.example.tickets.price.tax.TaxRateProvidingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class PassengerPriceCalculationServiceImpl implements PassengerPriceCalculationService {

    @Autowired
    private TaxRateProvidingService taxRateProvidingService;

    private final static BigDecimal CHILD_TICKET_PRICE_MULTIPLIER = new BigDecimal("0.5");
    private final static BigDecimal LUGGAGE_PRICE_MULTIPLIER = new BigDecimal("0.3");

    @Override
    public ResultPassengerPriceDTO calculateForPassenger(BigDecimal routePrice, PassengerDTO passenger) {
        BigDecimal currentTaxRate = taxRateProvidingService.getCurrentTaxRate();
        BigDecimal taxMultiplier = currentTaxRate.divide(BigDecimal.valueOf(100));

        BigDecimal ticketPrice = calculateTicketPriceByPassengerType(routePrice, passenger.getPassengerType(), taxMultiplier);
        BigDecimal luggagePrice = calculateLuggagePrice(routePrice, passenger.getLuggageCount(), taxMultiplier);
        BigDecimal totalPrice = ticketPrice.add(luggagePrice);
        return new ResultPassengerPriceDTO(passenger, ticketPrice, luggagePrice, totalPrice);
    }

    private BigDecimal calculateTicketPriceByPassengerType(BigDecimal routePrice, PassengerType passengerType, BigDecimal taxMultiplier) {
        switch (passengerType) {
            case ADULT:
                return calculatePriceWithAddedTax(routePrice, taxMultiplier);
            case CHILD: {
                BigDecimal price = routePrice.multiply(CHILD_TICKET_PRICE_MULTIPLIER);
                return calculatePriceWithAddedTax(price, taxMultiplier);
            }
            default:
                throw new IllegalArgumentException(String.format("Passenger type %s is unknown.", passengerType));
        }
    }

    private BigDecimal calculateLuggagePrice(BigDecimal routePrice, Integer luggageCount, BigDecimal taxMultiplier) {
        BigDecimal luggagePrice = routePrice.multiply(LUGGAGE_PRICE_MULTIPLIER).multiply(BigDecimal.valueOf(luggageCount));
        return calculatePriceWithAddedTax(luggagePrice, taxMultiplier);
    }

    private BigDecimal calculatePriceWithAddedTax(BigDecimal price, BigDecimal taxMultiplier) {
        BigDecimal tax = price.multiply(taxMultiplier);
        return price.add(tax).setScale(2, RoundingMode.HALF_UP);
    }
}
