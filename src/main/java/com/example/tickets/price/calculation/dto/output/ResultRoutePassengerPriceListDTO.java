package com.example.tickets.price.calculation.dto.output;

import com.example.tickets.price.base.dto.BasePriceDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@Getter
public class ResultRoutePassengerPriceListDTO {

    @JsonProperty("base_price")
    BasePriceDTO basePrice;
    @JsonProperty("passenger_price_list")
    List<ResultPassengerPriceDTO> resultPassengerPriceList;
    @JsonProperty("total_route_price")
    BigDecimal totalRoutePrice;
}
