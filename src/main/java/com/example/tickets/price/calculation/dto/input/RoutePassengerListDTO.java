package com.example.tickets.price.calculation.dto.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class RoutePassengerListDTO {

    @JsonProperty("route_name")
    private final String routeName;
    @JsonProperty("passenger_list")
    private final List<PassengerDTO> passengerList;
}
