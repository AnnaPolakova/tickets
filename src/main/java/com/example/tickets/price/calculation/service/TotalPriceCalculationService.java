package com.example.tickets.price.calculation.service;

import com.example.tickets.price.calculation.dto.output.ResultTotalPriceDTO;
import com.example.tickets.price.calculation.dto.input.RoutePassengerListDTO;

import java.util.List;

public interface TotalPriceCalculationService {

    ResultTotalPriceDTO calculateTotal(List<RoutePassengerListDTO> routePassengerList);
}
