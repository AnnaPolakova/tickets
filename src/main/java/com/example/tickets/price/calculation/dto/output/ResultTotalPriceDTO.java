package com.example.tickets.price.calculation.dto.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@Getter
public class ResultTotalPriceDTO {

    @JsonProperty("route_passenger_price_list")
    List<ResultRoutePassengerPriceListDTO> routePassengerPriceList;
    @JsonProperty("total_price")
    BigDecimal totalPrice;
}
