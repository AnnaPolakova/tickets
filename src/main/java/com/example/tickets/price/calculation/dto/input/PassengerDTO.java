package com.example.tickets.price.calculation.dto.input;

import com.example.tickets.price.calculation.PassengerType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PassengerDTO {

    @JsonProperty("passenger_type")
    private final PassengerType passengerType;
    @JsonProperty("luggage_count")
    private final Integer luggageCount;
}
