package com.example.tickets.price.tax;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class TaxRateProvidingServiceImpl implements TaxRateProvidingService {

    public static final BigDecimal CURRENT_TAX_RATE = new BigDecimal("21.0");

    @Override
    public BigDecimal getCurrentTaxRate() {
        return CURRENT_TAX_RATE;
    }
}
