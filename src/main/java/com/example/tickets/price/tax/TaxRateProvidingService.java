package com.example.tickets.price.tax;

import java.math.BigDecimal;

public interface TaxRateProvidingService {

    BigDecimal getCurrentTaxRate();
}
