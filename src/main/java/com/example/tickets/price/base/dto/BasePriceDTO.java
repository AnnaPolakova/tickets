package com.example.tickets.price.base.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@Getter
public class BasePriceDTO {

    @JsonProperty("route_name")
    private final String routeName;
    @JsonProperty("route_price")
    private final BigDecimal routePrice;
}
