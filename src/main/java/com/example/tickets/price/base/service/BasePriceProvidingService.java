package com.example.tickets.price.base.service;

import com.example.tickets.price.base.dto.BasePriceDTO;

public interface BasePriceProvidingService {

    BasePriceDTO getBasePriceByRouteName(String routeName);
}
