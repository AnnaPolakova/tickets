package com.example.tickets.price.base.service;

import com.example.tickets.price.base.dto.BasePriceDTO;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class BasePriceProvidingServiceImpl implements BasePriceProvidingService {

    public static final String VILNUS_LITHUANIA = "Vilnus, Lithuania";
    public static final String TALLIN_ESTONIA = "Tallin, Estonia";
    public static final String ROME_ITALY = "Rome, Italy";

    private static final List<BasePriceDTO> basePriceList = List.of(
            new BasePriceDTO(VILNUS_LITHUANIA, new BigDecimal("10.0")),
            new BasePriceDTO(TALLIN_ESTONIA, new BigDecimal("20.0")),
            new BasePriceDTO(ROME_ITALY, new BigDecimal("30.0"))
    );

    @Override
    public BasePriceDTO getBasePriceByRouteName(String routeName) {
        Map<String, BasePriceDTO> basePriceByRouteNameMap = basePriceList.stream()
                .collect(Collectors.toMap(basePrice -> basePrice.getRouteName(), Function.identity()));
        return basePriceByRouteNameMap.get(routeName);
    }
}
