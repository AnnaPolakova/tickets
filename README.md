# README #

To get ticket prices use ```localhost:8080/prices```

### Available routes and base prices

* Vilnus, Lithuania - 10
* Tallin, Estonia - 20
* Rome, Italy - 30

### Available passenger types

* ADULT
* CHILD

### Tax rate

For now the tax rate of 21% is always applied.

### Input example

```
[
    {
        "route_name": "Vilnus, Lithuania",
        "passenger_list": [
            {
                "passenger_type": "ADULT",
                "luggage_count": 2
            },
            {
                "passenger_type": "CHILD",
                "luggage_count": 3
            }
        ]
    },
    {
        "route_name": "Tallin, Estonia",
        "passenger_list": [
            {
                "passenger_type": "ADULT",
                "luggage_count": 1
            },
            {
                "passenger_type": "CHILD",
                "luggage_count": 0
            }
        ]
    }
]
```

### Output example

```
{
    "route_passenger_price_list": [
        {
            "base_price": {
                "route_name": "Vilnus, Lithuania",
                "route_price": 10.0
            },
            "passenger_price_list": [
                {
                    "passenger": {
                        "passenger_type": "ADULT",
                        "luggage_count": 2
                    },
                    "ticket_price": 12.10,
                    "luggage_price": 7.26,
                    "total_price": 19.36
                },
                {
                    "passenger": {
                        "passenger_type": "CHILD",
                        "luggage_count": 3
                    },
                    "ticket_price": 6.05,
                    "luggage_price": 10.89,
                    "total_price": 16.94
                }
            ],
            "total_route_price": 36.30
        },
        {
            "base_price": {
                "route_name": "Tallin, Estonia",
                "route_price": 20.0
            },
            "passenger_price_list": [
                {
                    "passenger": {
                        "passenger_type": "ADULT",
                        "luggage_count": 1
                    },
                    "ticket_price": 24.20,
                    "luggage_price": 7.26,
                    "total_price": 31.46
                },
                {
                    "passenger": {
                        "passenger_type": "CHILD",
                        "luggage_count": 0
                    },
                    "ticket_price": 12.10,
                    "luggage_price": 0.00,
                    "total_price": 12.10
                }
            ],
            "total_route_price": 43.56
        }
    ],
    "total_price": 79.86
}
```